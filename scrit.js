const btn = document.querySelector('button');
const list = document.querySelectorAll('li');
let size = 10;


// Pętla for
// const changeSize = () => {
//     size++;
//     for (let i = 0; i < list.length; i++) {
//         list[i].style.display = "block";
//         list[i].style.fontSize = `${size}px `;
//     }
// }


// forEach
const changeSize = () => {
    size++;
    list.forEach((li) => {
        li.style.display = "block";
        li.style.fontSize = `${size}px`;

    })
}

btn.addEventListener('click', changeSize)